package com.sda.fundamentals.annotations;

public @interface ComponentInject {  // modificator_de_acces @interface NumeAdnotare
    Class newClass();
    String name();
}
