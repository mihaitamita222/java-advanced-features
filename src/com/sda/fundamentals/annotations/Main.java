package com.sda.fundamentals.annotations;

import java.lang.reflect.InvocationTargetException;

/**
 * Annotations = metadata => ofera informatii despre un program, dar nu sunt parte din el;
 * Nu afecteaza in mod direct operatia codului la care fac referire
 * Informeaza compilatorul despre structura codului
 * Exista tool-uri care pot procesa adnotarile pentru a genera code, documentatie pe baza lor
 * Ele pot fi procesate la runtime -> ex: clasele pot fi gasite dupa adnotarile lor
 * Constau dintr-un caracter @ -> acesta ii spune compilatorului ca are de-a face cu o adnotare
 * Characterul @ este urmat de numele typului adnotarii
 * Ex. ComponentInject, DangerousPieceOfCode
 */

@ComponentInject(newClass = TestClass.class, name="testClass")
public class Main {

    private static TestClass testClass = new TestClass("test");

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        testClass.getClass().newInstance(); // deprecated is not good -> replace the piece of code with the new solution
        testClass.getClass().getDeclaredConstructor().newInstance();
        System.out.println(testClass.getTestField());
        testClass.methodThatThrowsAnException();
    }

    // Ignore when we're using a deprecated
    // (no longer important) method or type
    @SuppressWarnings("deprecation")
    static void callDeprecatedMethod() {
        testClass.testMethod();
    }

    static void callDeprecatedMethodWithoutAnnotation() {
        testClass.testMethod();
    }

}
