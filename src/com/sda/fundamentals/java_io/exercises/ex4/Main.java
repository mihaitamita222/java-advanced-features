package com.sda.fundamentals.java_io.exercises.ex4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Gaseste cel mai lung cuvand dintr-un fisier dat.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        // scanam fisierul dat
        Scanner scanner = new Scanner(new File("resources/exercise2.txt"));

        String longestWord = "";
        String current;

        while (scanner.hasNext()) {
            current = scanner.next();
            if (current.length() > longestWord.length()) {
                longestWord = current;
            }
        }

        System.out.println("Cel mai lung cuvant este: " + longestWord);
    }
}
