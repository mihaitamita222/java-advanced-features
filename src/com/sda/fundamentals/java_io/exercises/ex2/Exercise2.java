package com.sda.fundamentals.java_io.exercises.ex2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * 2. Create a file with a "lorem ipsum" paragraph within - it can be done by copy-pasting existing paragraph or generating it dynamically using Java library.
 * Read that file.
 * a). Count words
 * b). Count special signs (like comma, dot, spaces)
 * c). Select one word and print it's number of occurences
 */
public class Exercise2 {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("resources/exercise2.txt");
        List<String> fileLines = Files.readAllLines(path);

        int words = countWords(fileLines);
        int specialSigns = countSpecialSigns(fileLines);
        String word = "la";
        int noOfOcc = noOfOccForWord(word, fileLines);

        System.out.println("Words in the file: " + words);
        System.out.println("Special signs in the file: " + specialSigns);
        System.out.println("No of occurences of the word: " + word + " is: " + noOfOcc);
    }

    private static int countWords(List<String> fileLines) {
        int words = 0;
        for (String line : fileLines) {
            List<String> splittedLine = Arrays.stream(line.split(" ")).toList();

            for (String word: splittedLine) {
                if (word.equals(".") || word.equals(",")) {
                    // it;s not a word
                } else {
                    words++;
                }
            }

        }
        return words;
    }

    private static int countSpecialSigns(List<String> fileLines) {
        int specialSigns = 0;
        for (String line : fileLines) {
            String[] splittedLine = line.split(" ");
            specialSigns += splittedLine.length - 1; // count spaces

            for (String word: splittedLine) {
                if (word.contains(".") || word.contains(",")) {
                    specialSigns++;
                }
            }

        }
        return specialSigns;
    }

    private static int noOfOccForWord(String word, List<String> fileLines) {
        int count = 0;

        for (String line : fileLines) {
            String[] splittedLine = line.split(" ");
            for (String wordInLine: splittedLine) {
                if (word.equals(wordInLine)) {
                    count++;
                }
            }
        }

        return count;
    }


}
