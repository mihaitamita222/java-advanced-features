package com.sda.fundamentals.java_io.exercises.ex5;

import java.io.IOException;

/**
 * Create a program which will provide following features based on the Movie class objects:
 *      adding objects,
 *      returning object lists.
 * The Movie class should contain fields: title, genre, director, releaseYear.
 *
 * Adding objects should be written to a file.
 * Displaying object list should read the text file to convert individual lines to Movie objects.
 *
 * */
public class Main {

    public static void main(String[] args) throws IOException {
        MovieFileRepository movieFileRepository = new MovieFileRepository();

        movieFileRepository.add(new Movie("Star Wars Force Awaken", "Action", "J.J Ambrams", 2015));
        movieFileRepository.add(new Movie("Star Wars Last Jedi", "Action", "J.J Ambrams", 2017));

        System.out.println("Results :" + movieFileRepository.getAll());
    }

}
