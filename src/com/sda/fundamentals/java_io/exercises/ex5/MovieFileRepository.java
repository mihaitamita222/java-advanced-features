package com.sda.fundamentals.java_io.exercises.ex5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

class MovieFileRepository {

    private static final Path PATH = Paths.get("resources/ex5.txt");

    private final MovieParser movieParser = new MovieParser();

    public void add(Movie movie) throws IOException {
        Files.writeString(PATH, movieParser.toCSV(movie), StandardOpenOption.APPEND);
    }

    public List<Movie> getAll() throws IOException {
        List<String> movieLines = Files.readAllLines(PATH);
        List<Movie> movies = new ArrayList<>();
        for (String line : movieLines) {
            Movie movie = movieParser.fromCSV(line);
            movies.add(movie);
        }
        return movies;
    }

}