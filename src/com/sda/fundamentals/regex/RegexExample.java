package com.sda.fundamentals.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExample {
    public static void main(String[] args) {
        String text    =
                "This is the text to be searched " +
                        "for occurrences of the http:// pattern.";

        String regex = ".*http://.*";

        boolean matches = Pattern.matches(regex, text);

        System.out.println("matches = " + matches);

        // number of occurences of a word in a text
        String text1 = "This is the text which is to be searched for occurrences of the word 'is'.";
        String regex1 = "is";
        Pattern pattern1 = Pattern.compile(regex1);
        Matcher matcher1 = pattern1.matcher(text1);

        int count = 0;
        while(matcher1.find()) {
            count++;
        }
        System.out.println("found: " + count);
    }
}
