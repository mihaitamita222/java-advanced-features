package com.sda.fundamentals.generics.exercises.exercise1;

/**
 * Create a Person class that will implement a Comparable interface.
 * Person class should implement compareTo method, that eill verify if one person is taller than another.
 * Write a test class to teste the compareTo method displaying different messages about the compared objects.
 */
public class PersonTest {
    public static void main(String[] args) {
        Person p1 = new Person(180);
        Person p2 = new Person(165);

        if (p1.compareTo(p2) > 0) {
            System.out.println("Person1 is taller than person2");
        } else if (p1.compareTo(p2) < 0) {
            System.out.println("Person2 is taller than person1");
        } else {
            System.out.println("The two of the person provided are equally as height");
        }
    }
}
