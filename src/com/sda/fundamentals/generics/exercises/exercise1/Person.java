package com.sda.fundamentals.generics.exercises.exercise1;

public class Person implements Comparable<Person> {

    private int height;

    public Person(int height) {
        this.height = height;
    }

    @Override
    public int compareTo(Person otherPerson) {
        return this.height - otherPerson.height;
    }

    // method overloading
    public int compareTo(int otherHeight) {
        if (this.height < otherHeight){
            return -1;

        }
        else if (this.height> otherHeight){
            return 1;
        }
        else {
            return 0;

        }
    }
}
