package com.sda.fundamentals.generics.exercises.exercise2;

public class Bucket<T> {
    private T value;

    public Bucket(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
