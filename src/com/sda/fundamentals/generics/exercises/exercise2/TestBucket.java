package com.sda.fundamentals.generics.exercises.exercise2;

/**
 * Creati o simpla clasa Generica, care sa ne dea posibilitatea sa stocam
 * orice tip de valoare in interiorul ei.
 * Adauga un obiect de tipul String, un Integer si un Double intr-un array
 * de tipul Generic definit anterior.
 * Afiseaza toate valorile din array cu ajutorul unui for loop;
 */
public class TestBucket {
    public static void main(String[] args) {
        Bucket<String> stringItem = new Bucket<>("string_value");
        Bucket<Integer> integerItem = new Bucket<>(120);
        Bucket<Double> doubleItem= new Bucket<>(100.0);

        Bucket[] buckets =  new Bucket[] {stringItem, integerItem, doubleItem};

        for(int i = 0; i < buckets.length; i++) {
            System.out.println(buckets[i].getValue());
        }
    }
}
