package com.sda.fundamentals.generics.ex2;

/**
 *  Garage accepta doar obiecte ce extind clasa Vehicle
 *  Ne permite sa facem referire la clasa parinte, adica sa apelam proprietati, metode din clasa Vehicle
 *
 *  Putem apela doar metodele din clasa PARINTE, iar daca in copii avem alte metode nu putem sa le apelam
 */
public class Garage<T extends Vehicle> {
    private T vehicle;

    public Garage(T vehicle) {
        this.vehicle = vehicle;
    }

    public void repairVehicle() {
        vehicle.repair();
    }
}
