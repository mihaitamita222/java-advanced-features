package com.sda.fundamentals.generics.ex2;

public abstract class Vehicle {
    public abstract void repair();
}
