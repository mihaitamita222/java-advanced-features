package com.sda.fundamentals.generics.ex2;

public class Main {
    public static void main(String[] args) {
        Garage<Car> garage1 = new Garage<>(new Car());
        Garage<Vehicle> garage = new Garage<>(new Car());
//        Garage<Integer> garage = new Garage<>(new Integer(1));
//        Garage<String> garage = new Garage<>(new String("test"));
        garage1.repairVehicle();
        garage.repairVehicle();
    }
}
