package com.sda.fundamentals.generics.ex1;

public class GenericBox<T> {
    private T item;

    public GenericBox(T item) {
        this.item = item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }
}
