package com.sda.fundamentals.generics.ex1;

public class Main {
    public static void main(String[] args) {
        GenericBox<Car> boxWithACarInIt1 = new GenericBox<Car>(new Car());
        GenericBox<Car> boxWithACarInIt2 = new GenericBox<>(new Car());
        GenericBox<String> boxWithACarInIt3 = new GenericBox<>("test my string");
        GenericBox<Integer> boxWithACarInIt4 = new GenericBox<>(12);

        boxWithACarInIt1.getItem();
        boxWithACarInIt1.setItem(new Car());
    }
}
