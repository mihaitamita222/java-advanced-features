package com.sda.fundamentals.generics;

/**
 * Generics -> sunt cunoscute si sub denumirea de "templates"
 * <>  => se numeste operatorul diamond
 * fac posibila parametrizarea unei clase/metode/interfete, urmand sa specificam tipul exact atunci cand la folosim in cod
 *
 * Naming Conventions:
 * E - Element (used for Java Collection API)
 * T - Type   -> Clasa Type
 * K - Key type
 * N - Number type
 * V - Value type
 * S, U, V - second, third, fourth type -> daca sunt mai multe tipuri parametrizabile
 */
public class Main {
    public static void main(String[] args) {
        // pentru a crea si a invoca un obiect cu un tip generic, trebuie sa inlocuim litera T cu un tip specific de date, de ex. Integer, String etc
        Tea greenTea = new Tea();
//        Cup<Tea> teaCup1 = new Cup<Tea>(greenTea);
        Cup<Tea> teaCup = new Cup<>(greenTea);
        teaCup.drink();

        Coffee arabicCoffee = new Coffee();
        Cup<Coffee> coffeeCup = new Cup<>(arabicCoffee);
        Cup<Coffee> coffeeCup1 = new Cup<>(new Coffee());
        coffeeCup.drink();
    }


}
