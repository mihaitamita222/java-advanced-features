package com.sda.fundamentals.reflection.car;

public class Car {

    private String type;

    public Car() {
        System.out.println("We are in Car class constructor");
    }

    public String getType() {
        return "Audi";
    }

    private void getPrivateDetails() {
        System.out.println("This is my private method containing private details");
    }

}
