package com.sda.fundamentals.concurrency.exercises.ex4;

/**
 * Write a program which will synchronize access to a bank account. If any cyclical
 * Internet service wants to charge the account with a higher amount than currently
 * available, then the thread should be suspended. When additional money will be
 * transfered to the account, the thread should be raised.
 */
public class Main {

    public static void main(String[] args) {
        Account account = new Account(10000);
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    account.pay(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                account.transfer(5000);
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                account.transfer(6000);
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();
    }

}