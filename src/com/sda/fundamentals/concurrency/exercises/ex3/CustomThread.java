package com.sda.fundamentals.concurrency.exercises.ex3;


/**
 * 1. Create a class implementing the Runnable interface (implementing the run method):
 * a) Inside the run method display „Hello!”
 * b) Create a class object.
 * c) Start the thread receiving the created object as a parameter
 * (new Thread (<object>).start ())
 * d) Create several objects, run a separate thread for each of them.
 * e) Add the constructor to the created class, that accepts the int value.
 * f) For the displayed data inside the run method, add the received value (Hello + value).
 * g) Add a method to the class that will modify the int value.
 * h) Add a while loop to the run method, inside which it will print the modified int value
 * every few seconds.
 * i) Add the ability to disable (gracefully shutdown) the thread. Why shouldn’t we just „kill”
 * the thread?
 */
public class CustomThread implements Runnable {

    private Person person;
    private int value;

    public CustomThread(Person person, int value) {
        this.person = person;
        this.value = value;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + ": Hello " + value);

        while(value < 10) {
            System.out.println(Thread.currentThread().getName() + " -> Updated value: " + value);

            updateValue();

            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                System.out.println("Thread was interupted");
            }
        }
    }

    public void updateValue() {
        this.value++;
    }
}
