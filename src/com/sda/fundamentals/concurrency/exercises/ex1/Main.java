package com.sda.fundamentals.concurrency.exercises.ex1;

/**
 * Write a program that in parallel will find even numbers in two intervals:
 * 10-30
 * 50-70
 */
public class Main {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 10; i < 30; i++) {
                    if (i % 2 == 0) {
                        System.out.println(Thread.currentThread().getName() + " " + i);
                    }
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 50; i < 70; i++) {
                    if (i % 2 == 0) {
                        System.out.println(Thread.currentThread().getName() + " " + i);
                    }
                }
            }
        });

        thread1.start();
        thread2.start();

        CustomThread1 customThread1 = new CustomThread1();
        CustomThread2 customThread2 = new CustomThread2();
        customThread1.start();
        customThread2.start();
    }
}
