package com.sda.fundamentals.concurrency.exercises.ex2;

/**
 * Scrie un program care sa rezolve urmatoarea problema.
 *
 * Pe drumul dintre orasele A si B exista un pod (bridge) pe care poate sa treaca o singura masina odata.
 * Implementeaza un mecanism care sa permita accesul sincronizat al unui obiect de tipul Car pe un obiect din clasa Bridge
 *
 * Clasa Car ar trebui sa contina urmatoarele informatii: car name, car type
 * Clasa Bridge trebuie sa contina metoda "driveThrough", care o sa accepte ca parametru obiecte de tipul Car
 * Calatoria ar trebui sa dureze 5 secunde.
 */
public class Main {
    public static void main(String[] args) {
        Bridge bridge = new Bridge();
        Car car1 = new Car("VW", "Combi");
        Car car2 = new Car("SEAT", "Suv");
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                bridge.driveThrough(car1);
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                bridge.driveThrough(car2);
            }
        });
        thread1.start();
        thread2.start();
    }
}
