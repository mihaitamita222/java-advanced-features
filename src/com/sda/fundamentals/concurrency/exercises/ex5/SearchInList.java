package com.sda.fundamentals.concurrency.exercises.ex5;

import java.util.List;
import java.util.concurrent.Callable;

public class SearchInList implements Callable<String> {

    private final List<Integer> list;

    public SearchInList(List<Integer> list) {
        this.list = list;
    }

    public void search() {
        for (int number : list) {
            if (number == 10) {
                break;
            }
        }
    }

    @Override
    public String call() throws Exception {
        search();
        return "Number found in list";
    }
}
