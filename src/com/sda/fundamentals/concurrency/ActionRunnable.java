package com.sda.fundamentals.concurrency;

public class ActionRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("My runnable action");
    }
}
