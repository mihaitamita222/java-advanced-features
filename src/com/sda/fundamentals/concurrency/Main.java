package com.sda.fundamentals.concurrency;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLong;

public class Main {

    //un alt mechanism de synchronization pentru variabile pe langa, synchronized si atomics
    private volatile int count = 0;  // the value will be stored/read from the main memory

    private static int counter = 0;

    // thread ul principal intr-un program
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ActionThread action = new ActionThread();
        action.start();

        //linii de cod

        Thread actionRunnable = new Thread(new ActionRunnable());
        actionRunnable.start();

        // linii de cod

        CommunicationBetweenThreads dinner = new CommunicationBetweenThreads();

        Thread thread = new Thread() {
            @Override
            public void run() {
//                incrementCounter();
                dinner.prepareDinner();
            }
        };

        Thread threadRunnable = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
//                    incrementCounter();
                    dinner.waitForDinner();
                    Thread.sleep(6000); // 6000 ms => 6 sec
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
        threadRunnable.start();

        // Future - cum consumam rezultatul unui Future task?
        // isDone() si get()
        Future<Integer> future = new SquareCalculator().calculate(10);

        while(!future.isDone()) {
            System.out.println("Calculating...");
            Thread.sleep(300);
        }

        Integer result = future.get();
        System.out.println("Result:" + result);

        // atomic
        counter();

    }

    static class SquareCalculator {

        private ExecutorService executor = Executors.newSingleThreadExecutor(); // ne pune la dispozitie un singur thread
        private ExecutorService executor1 = Executors.newFixedThreadPool(10);  // new pune la dispozitie 10 thread uri

        // metoda care ruleaza async
        public Future<Integer> calculate(Integer input) {
            return executor.submit(() -> {
                Thread.sleep(1000);
                return input * input;
            });
        }
    }

    static void counter() throws InterruptedException, ExecutionException {
        // exemple de variabile atomic -> atomic = synchronizes (doar un thread odata poate sa modifice valoarea)
        AtomicInteger atomicInteger = new AtomicInteger(100);
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        AtomicLong atomicLong = new AtomicLong(100L);
        int[] array1 = new int[] {1, 2, 3};
        AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(array1);

        // toate operatiile efectuate pe variabilele de mai sus sunt synchronized

        Callable<Integer> callable1 = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return atomicInteger.addAndGet(1);
            }
        };

        Callable<Integer> callable2 = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return atomicInteger.decrementAndGet();
            }
        };

        Callable<Integer> callable3 = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return atomicInteger.addAndGet(20);
            }
        };

        List<Callable<Integer>> actions = Arrays.asList(callable1, callable2, callable3);

        ExecutorService atomicExecutorService = Executors.newFixedThreadPool(5); // creaza 5 thread uri
//        Future<Integer> future1 = atomicExecutorService.submit(callable1);
//        Future<Integer> future2 = atomicExecutorService.submit(callable2);
//        Future<Integer> future3 = atomicExecutorService.submit(callable3);
//        List<Future<Integer>> submitResults = Arrays.asList(future1, future2, future3);

        List<Future<Integer>> results = atomicExecutorService.invokeAll(actions);  // similar cu submit(), doar ca invokeAll() ne permite sa realizam mai multe actiuni deodata

        Integer firstSuccessfullyCompletedResult = atomicExecutorService.invokeAny(actions); // returneaza rezultatul task-ului ce s-a realizat primul cu succes

        for (Future<Integer> result : results) {
            System.out.println(result.get());
        }
    }

}
