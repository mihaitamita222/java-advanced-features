package com.sda.fundamentals.concurrency;

public class ActionThread extends Thread {
    @Override
    public void run() {
        System.out.println("My thread action");
    }
}
