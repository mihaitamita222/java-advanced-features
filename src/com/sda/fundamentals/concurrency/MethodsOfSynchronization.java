package com.sda.fundamentals.concurrency;

public class MethodsOfSynchronization {

    // sincronizarea metodei
    public synchronized void start(int numberOfElements) {
        for (int i = 0; i < numberOfElements; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i + 1);
        }
    }

    // sincronizarea blocului de cod
    public void line(int number) {
        System.out.println("Test block synchronization" + number);

        synchronized (this) {
            try {
                number++;
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e);
            }
            System.out.println("Wohooo!");
        }
    }

}