package com.sda.fundamentals.collections.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapExamples {
    public static void main(String[] args) {
        // Map<Key, Value> => este un dictionar cheie-valoare, care nu permite duplicat de cheie

        //creating an object of the Map type
//        Map<String, Float> employees111 = new HashMap<String, Float>();
        Map<String, Float> employees = new HashMap<>();

        //adding items to the map
        employees.put("John Dool", 3000.0f); // put -> adauga un element in Map
        employees.put("Chao Di", 4500.0f);
        employees.put("Prasad D", 1000.0f);
        employees.put("Prasad D", 101.0f); // daca adaug o cheie care este prezenta in map, valoarea celei vechi va fi suprascrisa

        // este bine sa verificam daca cheia este prezenta in Map inainte sa adaugam o noua cheie
        if (employees.containsKey("Prasad D")) {
            // daca este in map
        } else {
            // daca nu este in map
        }

        // putIfAbsent => verifica daca cheia este prezenta in Map, daca nu atunci o adauga, daca este atunci merge mai departe
        employees.putIfAbsent("Empl1",  100.0f);

        //removing an element from the map with a given key
        employees.remove("John Dool"); //automat se sterge si valoarea de la acea cheie

        //retrieving an element from the map with a given key
        System.out.println(employees.get("Prasad D")); // get() va transforma key in hashcode si acceseaza direct valoare pentru respectiva cheie, nu face un for peste map! apoi returneaza valoarea

        // replace va returna valoarea veche pe care a inlocuit-o;
        employees.replace("Empl1", 200.0f); // inlocuieste valoarea aflata la key ul dat

        int mapSize = employees.size(); // returneaza dimensiunea unui map

//        employees.clear(); // sterge tot din map (toate perechile key-value)


        // adaugarea unei valori pentru o cheie existenta
        if(employees.containsKey("Empl1")) {
            employees.put("Empl1", 1000.0f); // se inlocuieste valoarea veche, cu noua valoare
        }

        // iterare peste chei
        for (String key : employees.keySet()) {
            System.out.println("Employee: " + key);
        }

        // iterare peste valorile din mapa
        for (float value : employees.values()) {
            System.out.println("Payment: "+ value);
        }

        // entrySet() => transforma map-ul intr-un set Set<Map.Entry<String, String>> entrySet
        for (Map.Entry<String, Float> pair : employees.entrySet()) {
            System.out.println("Employee-payment " + pair);
        }

        // another example
        Map<String, String> countries = new HashMap<>();
        countries.put("Poland", "Warsaw");
        countries.put("Germany", "Berlin");

        for (Map.Entry<String, String> dictionary : countries.entrySet()) {
            String country = dictionary.getKey();
            String capital = dictionary.getValue();
            System.out.printf("%s : %s\n", country, capital);
        }

        // Iterator
        Iterator<Map.Entry<String, Float>> iteratorMap = employees.entrySet().iterator();

        // .hasNext() => daca mai exista un element, atunci mergi mai departe
        while(iteratorMap.hasNext()) {
            // .next() => acceseaza elementul
            Map.Entry<String, Float> entry = iteratorMap.next();

            System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
        }

    }
}
