package com.sda.fundamentals.collections.set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExamples {
    public static void main(String[] args) {
        Set<String> travelRoute = new HashSet<>();
//        Set<String> travelRouteSol2 = new HashSet<String>();
        Set<String> travelRoute1 = new LinkedHashSet<>(); // first si last
        Set<String> travelRoute2 = new TreeSet<>(); // este o colectie de date ordonata indiferent de situatie

        travelRoute.add("Berlin");
        travelRoute.add("Madrid");

        travelRoute.remove("Berlin"); // remove returneaza true sau false
        if (travelRoute.remove("Berlin")) {
            System.out.println("Am sters cu success valoarea data");
        } else {
            System.out.println("Nu am reusit sa sterg valoarea transmisa; fie nu exista; fie nu este potrivita");
        }

        for (String country : travelRoute) {
            System.out.println(country);
        }

        System.out.println("Size of travel set is: " + travelRoute.size()); // cate elemente sunt in set

        travelRoute.clear(); // sterge toate elementele din set

        for (String country : travelRoute) {
            System.out.println(country);
        }

        Set<String> names = new HashSet<>();
        names.add("John");
        names.add("Neeki");
        names.add("Packia");
        names.add("John");
        if (names.contains("John")){
            System.out.println("Element exists");
        }
        for (String name : names) {
            System.out.println(name);
        }

        // toArray() -> transforma set-ul intr-un array de elemente
        String[] namesArray = (String[]) names.toArray();
    }
}
