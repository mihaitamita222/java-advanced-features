package com.sda.fundamentals.collections.queue;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

// LIFO (Last In, First Out)
public class LifoQueueExample {
    public static void main(String[] args) {
        Queue<String> queue = new PriorityQueue<>();
        queue.offer("test1");
        queue.offer("test2");
        System.out.println(queue.poll());
        System.out.println(queue.poll());

        //
        Stack<String> stack = new Stack<>();
        stack.push("item1");
        stack.push("item2");
        System.out.println(stack.pop());
        System.out.println(stack.pop());
    }
}
