package com.sda.fundamentals.collections.list;

import java.util.*;

public class ListExamples {
    public static void main(String[] args) {
        // List este o interfata nu putem crea instante, de aceea folosim o clasa concreta pentru a crea o lista
        List<Float> transfers = new ArrayList<>();
        List<Float> transfers1 = new LinkedList<>();
        List<Float> transfers2 = new Vector<>();
        transfers.add(129.0f);
        transfers.add(400.0f);
        transfers.add(5000.0f);
        transfers.add(400.0f);  // duplicated element

        for (Float transfer : transfers) {
            System.out.println(transfer);
        }

        for (int i = 0; i < transfers.size(); i++) {  // size() -> returneaza lungimea listei
            System.out.println(transfers.get(i)); // get(i) returneaza elementul aflat la index ul i - pozitia i
        }

        transfers.set(1, 121.0f); // setam o valoare la un anumit index

        // elementele unei liste au indecsi bazati pe 0 - ca si la arrays
        System.out.println("Get element found at index 1: " + transfers.get(1));

        transfers.remove(129.0f); // remove an element from the list
        for (Float transfer : transfers) {
            System.out.print(transfer + " ");
        }
        System.out.println();

        // List using Iterator
        System.out.print("Print the elements of an arry using iterator: ");
        Iterator<Float> iterator = transfers.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }

        System.out.println();

    }
}
