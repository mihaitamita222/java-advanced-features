package com.sda.fundamentals.collections.list.exercises;

import java.util.List;

/**
 * Ratings received. Display their average. The nunmbers can not be less than 1 and greater than 6.
 */
public class Exercise2 {
    public static void main(String[] args) {
        average(List.of(1, 2, 3, 4, 5, 6));
    }

    static void average(List<Integer> ratings) {
        double sum = 0;
        int count = 0;
        for (int rating: ratings) {
            if (rating > 1 && rating < 6) {
                sum += rating;
                count++;
            }
        }
        System.out.println("The average of the ratings between 1 and 6 is: " + sum / count);
    }
}
