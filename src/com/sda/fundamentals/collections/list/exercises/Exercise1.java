package com.sda.fundamentals.collections.list.exercises;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * 1. Creaza o Lista si afiseaza rezultatele (datele trebuie introduse de la tastatura)
 * a) Lista de cumparaturi - daca un element exista in list, atunci nu ar mai trebui sa il adaugam
 * b) Adauga posibilitatea de stergere a unui element pus in lista de cumparaturi
 * c) Afiseaza toate elementele din lista
 * d) Afiseaza doar acele elemente care incep cu litera "m" (e.g. milk)
 * e). Afiseaza doar acele elemente care sunt urmate de un element care incepe cu "m" (e.g. eggs, daca milk este urmatorul element de pe lista)
 */
public class Exercise1 {
    public static void main(String[] args) {
        ArrayList<String> purchases = new ArrayList();

        System.out.println("Please enter '0' to exit");
        System.out.println("Enter 9 if you want to delete an element from your list");
        System.out.println("Please enter option: ");

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();

        if (option == 0) {
            System.out.println("Program has been terminated.");
        }

        do {
            scanner = new Scanner(System.in);
            System.out.println("Please enter chores: ");
            String purchase = scanner.nextLine();
            if (!purchases.contains(purchase)) {
                purchases.add(purchase);
            }
            scanner = new Scanner(System.in);
            System.out.println("Please enter option: ");
            option = scanner.nextInt();

            // remove an existing element
            if (option == 9) {
                System.out.println("Enter the element you want to delete:");
                scanner = new Scanner(System.in);
                String toRemove = scanner.nextLine();
                purchases.remove(toRemove);
                System.out.println(toRemove + " element was already removed from your list");
                // enter the option again
                scanner = new Scanner(System.in);
                System.out.println("Please enter option: ");
                option = scanner.nextInt();
            }
        } while (option != 0);

        System.out.println("Displaying the purchases entered...");
        for(String purchase : purchases) {
            System.out.println(purchase);
        }

        System.out.println("Displaying the purchases that start with m...");
        for(String purchase : purchases) {
            if (purchase.startsWith("m")) {
                System.out.println(purchase);
            }
        }

        System.out.println("Displaying the purchases whose next product on the list starts with m...");
        for (int i = 0; i < purchases.size() - 1; i++) {
            if (purchases.get(i+1).startsWith("m")) {
                System.out.println(purchases.get(i));
            }
        }

    }
}
