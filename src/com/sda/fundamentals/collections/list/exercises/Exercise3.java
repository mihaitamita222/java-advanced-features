package com.sda.fundamentals.collections.list.exercises;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// List of lists - multiplication table
public class Exercise3 {
    public static void main(String[] args) {
        List<List<Integer>> multiplicationTable = new ArrayList<>();
        multiplicationTable.add(0, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(1, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(2, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(3, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(4, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(5, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(6, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(7, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(8, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        multiplicationTable.add(9, List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));

        for (int i = 0; i < multiplicationTable.size(); i++) {
            Iterator<Integer> iterator = multiplicationTable.get(i).iterator();
            while (iterator.hasNext()) {
                System.out.print(i * iterator.next() + " ");
            }
            System.out.println();
        }
    }
}
