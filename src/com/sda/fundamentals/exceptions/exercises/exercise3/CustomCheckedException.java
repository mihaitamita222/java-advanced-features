package com.sda.fundamentals.exceptions.exercises.exercise3;

public class CustomCheckedException extends Exception {

    public CustomCheckedException(String message) {
        super(message);
    }

}
