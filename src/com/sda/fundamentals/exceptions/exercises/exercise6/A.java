package com.sda.fundamentals.exceptions.exercises.exercise6;

public class A {
    void method() throws CustomExceptionBase {
        throw new CustomExceptionBase("CustomExceptionBase");
    }
}
