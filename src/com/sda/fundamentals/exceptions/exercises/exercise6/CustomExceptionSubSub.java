package com.sda.fundamentals.exceptions.exercises.exercise6;

public class CustomExceptionSubSub extends CustomExceptionSubBase {
    public CustomExceptionSubSub(String message) {
        super(message);
    }
}
