package com.sda.fundamentals.exceptions.exercises.exercise6;

public class CustomExceptionBase extends Exception {
    public CustomExceptionBase(String message) {
        super(message);
    }
}
