package com.sda.fundamentals.exceptions.exercises.exercise6;

public class C extends B {
    @Override
    void method() throws CustomExceptionSubSub {
        throw new CustomExceptionSubSub("CustomExceptionSubSub");
    }
}
