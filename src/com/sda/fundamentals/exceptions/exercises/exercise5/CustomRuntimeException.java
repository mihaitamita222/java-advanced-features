package com.sda.fundamentals.exceptions.exercises.exercise5;

public class CustomRuntimeException extends RuntimeException {
    public CustomRuntimeException(String message) {
        super(message);
    }
}
