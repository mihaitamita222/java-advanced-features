package com.sda.fundamentals.exceptions.exercises.exercise5;

public class Exercise {
    void f() throws CustomCheckedException {
        try {
            g();
        } catch (RuntimeException e) {
            throw new CustomCheckedException("Test custom checked exception");
        }
    }

    void g() {
        throw new CustomRuntimeException("Test custom exception");
    }
}
