package com.sda.fundamentals.exceptions.exercises.exercise5;

public class CustomCheckedException extends Exception {
    public CustomCheckedException(String message) {
        super(message);
    }
}
