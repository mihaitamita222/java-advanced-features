package com.sda.fundamentals.exceptions.exercises.exercise7;

import java.util.Scanner;

/**
 * Creati o noua clasa numita ParseInt, care sa citeasca de la tastatura un string de forma:
 * 10 20 30 40
 * Convertiti valorile citite de la tastatura in numere intregi si efectuati suma lor.
 * Afisati rezultatul in consola.
 * Exemplu:
 * Input: 10 20 30 40
 * Output: The sum of the integers on the line is 100.
 * Incercti alte valori data ca si input.
 * Apoi incercati sa scrieti o linie care contine atat numere cat si alte valori, ex:
 * Input: We have 2 dogs and 1 cat.
 * Care este Output-ul acestui input?
 * Cum putem rezolva problema?
 */
public class ParseInts {
    public static void main(String[] args) {
        int sum = 0;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a line of text: ");

//        try {
//            while (scan.hasNext()) {
//                sum = 0;
//                String line = scan.nextLine();
//                for (String el : line.trim().split(" ")) {
//                    sum+= Integer.parseInt(el);
//                }
//                System.out.println("The sum of the integers on this line is " + sum);
//            }
//        } catch (NumberFormatException e) {
//        }

//        vs


        while (scan.hasNext()) {
            sum = 0;
            String line = scan.nextLine();
            for (String el : line.trim().split(" ")) {
                try {
                    sum += Integer.parseInt(el);
                } catch (NumberFormatException e) {
                }
            }
            System.out.println("The sum of the integers on this line is " + sum);
        }

//        Scanner scanLine = new Scanner(scan.nextLine());

//        try {
//            while (scanLine.hasNext()) {
//                sum += Integer.parseInt(scanLine.next());
//            }
//        } catch (NumberFormatException ex) {
//            System.out.println("error!");
//        }

        // vs.

//        while (scanLine.hasNext()) {
//            try {
//                sum += Integer.parseInt(scanLine.next());
//            } catch (NumberFormatException ex) {
//
//            }
//        }

//        System.out.println();
//        System.out.println("The sum of the integers on this line is " + sum);
    }
} 