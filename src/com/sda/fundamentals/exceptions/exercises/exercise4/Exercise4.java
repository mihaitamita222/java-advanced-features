package com.sda.fundamentals.exceptions.exercises.exercise4;

// Define an object reference and initialize it to null. Try to call a method through this reference. Now wrap the code in a try-catch clause to catch the exception.
public class Exercise4 {
    public static void main(String[] args) {
        String test = null;
//        test.isBlank();
        try {
            test.isBlank();
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }
}
