package com.sda.fundamentals.exceptions.exercises.exercise1;

import java.util.InvalidPropertiesFormatException;
import java.util.Scanner;

/**
 * Scrie o aplicatie care sa citeasca un input de la tastatura
 * si sa afiseze in consola valoarea furnizata de utilizator,
 * foloseste try-catch pentru a parsa input-ul.
 * Exemple:
 * Input: 10
 * Output: int -> 10
 *
 * Input: 10.0
 * Output: double -> 10.0
 *
 * Input: "Hello!"
 * Output: "Hey! That's not a value! Try once more."
 *
 * Ce parere ai despre aruncarea unei exceptii: cand ar trebui sa aruncam o exceptie si cand sa returnam Null in cazul
 * unui esec al unei metode. Incearca sa implementezi o metoda pentru fiecare situatie.
 */
public class Exercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("\nEnter a value: ");
        String input = scanner.nextLine();

        try {
            if (input.contains(".")) {
                System.out.println("double -> " + Double.parseDouble(input.trim()));
            } else {
                System.out.println("int -> " + Integer.parseInt(input.trim()));
            }
        } catch (NumberFormatException e) {
            System.out.println("Hey! That's not a value! Try once more.");
        }

    }


    private static void methodThatBestThrowAnException(Person person) throws InvalidPropertiesFormatException {
        if (person.getUsername() == null ||
                person.getUsername().isBlank()) {
            throw new InvalidPropertiesFormatException("Username should be present");
        }

        // save person into database
    }

    private static Person methodThatBestReturnNull(String username) {
        // search for person by username into database
        // if found then return person, else return null
        boolean found = false;
        return found ?
                new Person(username, "first name", "last name")
                :
                null;
    }


    static class Person {
        private String username;
        private String firstName;
        private String lastName;

        public Person(String username, String firstName, String lastName) {
            this.username = username;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getUsername() {
            return username;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
    }

}
