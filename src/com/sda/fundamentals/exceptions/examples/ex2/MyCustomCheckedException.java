package com.sda.fundamentals.exceptions.examples.ex2;

// custom checked exception
public class MyCustomCheckedException extends Exception {
    // poate sa existe si fara constructor
    public MyCustomCheckedException(String message) {
        super(message);
    }
}
