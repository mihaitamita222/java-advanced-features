package com.sda.fundamentals.exceptions.examples.ex2;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        method();
//        System.out.println("Aici nu se mai ajunge...");
        System.out.println("next line....");

        try {
            // cod care stim ca s-ar putea sa arunce exceptie
        } catch (Exception e) {
            System.out.println("exection");
        }

        int y = 1;
        int array[] = new int[3];
        array[0] = 1;
        array[1] = 2;
        array[2] =3;
        int index = 2;
        String string = null;

        try {
            int x = array[index] / y;
            System.out.println(string.toString());
        } catch (ArithmeticException e) {
            System.out.println("Arithmetic exception caught!");
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Out of bound");
        } catch (Exception e) {
            System.out.println("another exception");
        }

        try {
            int x = array[index] / y;
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException  e) {
            System.out.println("two types of exception caught");
        }

//        methodThrowingARuntimeExceptionOrUnchecked(); // nu suntem obligati sa ii facem handling, sa o tratam

        try {
            methodThrowingARuntimeExceptionOrUnchecked();
        } catch (RuntimeException e) {
            System.out.println("Handled exception");
        }

//        methodThrowingACheckedException();

        try {
            method1();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            method1();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    static void method() {
        try {
            int array[] = new int[3];
            array[0] = 1;
            array[1] = 2;
            array[2] =3;
            String str = null;
            str.isBlank();
            array[10] = 10;
            int a = 10/0;
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Handling index out of bounds exception..");
        } catch (ArithmeticException e) {
            System.out.println("Divide by zero");
        } catch (RuntimeException e) {
            System.out.println("Runtime exception");
        } finally {
            System.out.println("Will be executed no matter what");
        }

    }

    static void methodThrowingARuntimeExceptionOrUnchecked() {
        throw new RuntimeException();
    }

    static void methodThrowingACheckedException() throws Exception {
        throw new Exception();
    }

    static void method1() throws NoSuchMethodException, ClassNotFoundException, FileNotFoundException {
        method2();
    }

    static void method2() throws ClassNotFoundException, FileNotFoundException {
        method3();
    }

    // aruncarea exceptiilor
    static void method3() throws FileNotFoundException {
        throw new FileNotFoundException();
    }

}
