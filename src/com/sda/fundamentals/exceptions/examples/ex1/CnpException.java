package com.sda.fundamentals.exceptions.examples.ex1;

// Exception este copilul lui Throwable
public class CnpException extends Exception {

    public CnpException(String message) {
        // apelam constructorul din clasa parinte
        super(message);
    }
}