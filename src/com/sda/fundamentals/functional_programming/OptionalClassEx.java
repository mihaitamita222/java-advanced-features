package com.sda.fundamentals.functional_programming;

import java.util.Optional;

public class OptionalClassEx {

    public static void main(String[] args) {
//        String str1 = null;
        String str1 = "value is present";
        Optional<String> optionalStr1 = Optional.of(str1); // daca str1 ar fii egal cu null atunci of() arunca un NPE

        String str2 = null;
        Optional<String> optionalStr2 = Optional.ofNullable(str2); // daca valoarea ii null, valoarea o sa devina Optional.EMPTY
//        Optional<String> optStrWithOf = Optional.of(str2);
        optionalStr2.ifPresent((val) -> System.out.println(val));
        optionalStr2.ifPresent(val -> System.out.println(val));
        optionalStr2.ifPresent(System.out::println); // method reference

        System.out.println(optionalStr1.get()); // afisam valoarea string-ului din interiorul Optinal ului, nu este indicat sa il folositi deoarece arunca o exceptie daca valoarea este null
//        System.out.println(optionalStr2.get()); // o sa arunce o exceptie
        if (optionalStr2.isPresent()) {  // cand folosim get(), deoarece poate arunca NPE, trebuie sa verificam daca exista valoare
            String valueFromOptional = optionalStr1.get();
            System.out.println(optionalStr1.get());
        }

        if (optionalStr2.isEmpty()) {
            System.out.println("The value of str2 is empty");
        }

        if (optionalStr2.isPresent()) {
            System.out.println(optionalStr2.get());
        }

        // folositi in schimbul .get() -ului, orElse(), orElseGet() sau orElseThrow()
        String strValueOrDefault = optionalStr2.orElse("Default value");
        System.out.println(optionalStr1.orElse("Default value -> Value found at str1 was null")); // returneaza valoarea daca o gaseste, altfel returneaza string ul default pasat lui orElse()
        System.out.println(optionalStr2.orElse("Default value -> Value found at str2 was null")); // returneaza valoarea daca o gaseste, altfel returneaza string ul default pasat lui orElse()

        System.out.println(optionalStr2.orElseGet(() -> optionalStr1.orElse("other")));

        System.out.println("Has optionalStr2 value? " + optionalStr2.isPresent());
        System.out.println("Is optionalStr2 empty? " + optionalStr2.isEmpty());

        optionalStr2.ifPresent(val -> System.out.println(val));
        optionalStr2.ifPresentOrElse(
                System.out::println,
                () -> System.out.println("Value is not present")
        );


    }
}
