package com.sda.fundamentals.functional_programming;

import java.util.Random;
import java.util.function.*;

public class FunctionalInterfacesExample {
    public static void main(String[] args) {
        // using existing functionalInterfaces from Java 8

        // Predicate<T>
        Predicate<String> startsWithAbcTest = s -> s.startsWith("ABC"); // solutia simplificata a urmatorului cod
//        Predicate<String> startsWithAbcTest = new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return s.startsWith("ABC");
//            }
//        };
        System.out.println(startsWithAbcTest.test("ABCDEF"));
        System.out.println(startsWithAbcTest.test("CDEF"));

        Predicate<String> isBlankString = s -> s.isBlank(); // predicate with lambda opertor
        System.out.println(isBlankString.test(""));

        // Function<T, R> , T - the type of the parameter, R - the return type
        Function<String, Integer> stringLengthFunction = s -> s.length(); // method apply returns Integer and the type of the param is String
        System.out.println(stringLengthFunction.apply("ABCDEF"));

        Function<String, String> replaceCommasWithDotsFunction = s -> s.replace(",", ".");
        System.out.println(replaceCommasWithDotsFunction.apply("a,b,c"));

        // Supplier<T>
        Supplier<Integer> randomNumberSupplier = () -> new Random().nextInt(0, 100); // nextInt returneaza un numar intreg random/aleatoriu intre 0 si 100
        int randomNumber = randomNumberSupplier.get();
        System.out.println("Random number: " + randomNumber);

        // Consumer<T>
        Consumer<Double> printWithPrefixConsumer = d -> System.out.println("Value: " + d);
        printWithPrefixConsumer.accept(10.5);

        // UnaryOperator<T>
        UnaryOperator<Integer> toSquareUnaryOp = i -> i * i;
        System.out.println(toSquareUnaryOp.apply(3));

        // lambda with block of code
        UnaryOperator<Integer> toSquareUnaryWithBlock = i -> {
            int result = i * i;
            System.out.println("Result: " + result);
            return result;
        };
        toSquareUnaryWithBlock.apply(30);

    }
}
