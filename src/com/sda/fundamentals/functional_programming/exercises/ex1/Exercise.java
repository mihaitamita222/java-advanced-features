package com.sda.fundamentals.functional_programming.exercises.ex1;

import java.util.Arrays;
import java.util.List;

/**
 * 1. Folosind expresiile lambda, calculati suma elementelor dintr-o lista de intregi data
 */
public class Exercise {

    public static void main(String[] args) {

        ArraysSum arraysSum = list -> {
            int sum = 0;
            for (int el : list) {
                sum += el;
            }
            System.out.println("The sum of the elements is: " + sum);
            return sum;
        };

        arraysSum.sum(List.of(1, 2, 3, 4, 5));
        arraysSum.sum(Arrays.asList(1, 2, 3, 4, 5, 6));

    }

}
