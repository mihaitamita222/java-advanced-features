package com.sda.fundamentals.functional_programming.exercises.ex1;

import java.util.List;

public interface ArraysSum {

    int sum(List<Integer> input);

}
