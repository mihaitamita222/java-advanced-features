package com.sda.fundamentals.functional_programming;

// Expresiile Lambda - introduse in Java 8 => o reprezentare a unei implementari a unei interfete care are o singura metoda abstracta
// Expresiile Lambda -> permit unei clase anonime sa fie tratata ca si o functie ordinara, prescurtand sintaxa definirii ei
// () inseamna semnatura unei metode, care poate sa aibe si parametrii, (param1, param2)
// -> inseamna Operatorul Lambda
// ce urmeaza dupa Operatorul Lambda reprezinta implementarea metodei abstracte
// (parameterName) -> expresionToEvaluate
public class LambdaExpressions {

    public static void main(String[] args) {

        // exemplul 1
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Hello!");
            }
        };

        // the code from above is similar with the next syntax

        Runnable r2 = () -> System.out.println("Hello!");
        Runnable r3 = () -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hello!");
        };

        // exemplul 2
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("We are in method run from thread 1.");
            }
        });

        // codul de mai sus este similar cu urmatoarea sintaxa

        Thread thread2 = new Thread(() -> System.out.println("We are in method run from thread 2."));

        thread1.start();
        thread2.start();

        // exemplul 3
        FunctionalInterface objectCreatedWithAnonymousClass = new FunctionalInterface() {
            @Override
            public void singleAbstractMethod(String text) {
                System.out.println("The text is: " + text);
            }
        };

        FunctionalInterface objectCreatedWithLambda = (text) -> System.out.println("The text is: " + text);
        FunctionalInterface objectCreatedWithLambda2 = text -> System.out.println("The text is: " + text); // pentru ca avem un singur parametru, parantezele rotunde nu sunt necesare
        objectCreatedWithLambda.singleAbstractMethod("my custom text");

    }

}
