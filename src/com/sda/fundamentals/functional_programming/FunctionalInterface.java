package com.sda.fundamentals.functional_programming;

import java.util.concurrent.Callable;

// Interfetele functionala - pot sa aiba doar o singura metoda abstracta
// SAM interfaces => Single Abstract Method
// pot sa contina si metode default (0, 1 sau mai multe)
// ex. Runnable, Callable
public interface FunctionalInterface {

    void singleAbstractMethod(String text);

    // this kind of methods are allowed in functional interfaces
    default void defaultMethod() {
        System.out.println("This is a default method with a default implementation");
    }

}
