package com.sda.fundamentals.oop.taxonomy;

// top level class -> not included in another class
// not top level = nested
public class ClassTaxonomy {
    // class members -------------------

    // fielduri de instantanta
    private String name; // apel: instanta.name; => unde (instanta = new ClassTaxonomy();)
    // fielduri de clasa
    private static String classification; // apel: ClassTaxonomy.classification
    private static final String NO_OF_CLASS = "value"; // apel: ClassTaxonomy.NO_OF_CLASS

    // metoda
    public String getName() {
        return this.name;
    }

    public void doSomething() {
//        NO_OF_CLASS = "bla bla"; // won't compile because it's declared final => constanta
        // variabile locale
        int no;
        for(int i = 0; i < 10; i++) { // i=> variabila locala

        }
        // i nu mai este vizibil
    }


    // nested class -> static class
    static class FirstChildClassTaxonomy {
        private void doSmth() {
//             name = "ana"; // won't compile
            classification = "classif";
        }
    }

    // nested classes -> inner class (non-static)
    class ChildClassTaxonomy {
        private void doSmth() {
            name = "ana";
            classification = "classif";
        }
    }

    void getTaxonomy(String type) {
        // local class - declared inside of a code block (a method in this case)
        class Classification {
            String type;
        }
        Classification classification = new Classification();
        classification.type = type;
    }

    // inner class -> anonymous class
    void anonymousClassExample() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Run");
            }
        };
        runnable.run();
    }

    // end of class members -------------------
}
