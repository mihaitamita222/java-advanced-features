package com.sda.fundamentals.oop.abstractClass;

public class TestClasses {

    public static void main(String[] args) {
//        AbstractCarClass abstractCarClass = new AbstractCarClass(); // invalid code -> abstract class cannot be instantiated
        ConcreteClassOfAbstract concreteCarClass = new ConcreteClassOfAbstract("WWWZZZ123");
        concreteCarClass.runEngine();
        AbstractCarClass carClass = concreteCarClass;
        carClass.runEngine();
        AbstractCarClass abstractCarClass = new ConcreteClassOfAbstract("23423");
        abstractCarClass.runEngine();
    }
}
