package com.sda.fundamentals.oop.abstractClass;

public class ConcreteClassOfAbstract extends AbstractCarClass {

    public ConcreteClassOfAbstract(String vin) {
        super(vin);
    }

    @Override
    public void runEngine() {
        System.out.println("Inside run engine method");
    }
}
