package com.sda.fundamentals.oop.abstractClass;

// abstract class -> nu pot fi instantiate
// trebuie sa fie extinse pentru a fi folosite
// poate sau nu sa includa metode abstracte - metode fara implementare
// o clasa abstracta poate sa nu aiba nici o metoda abstracta, invers nu este posibil
// field-urile, metodele statice si non-abstract - functioneaza ca in clasele non-abstract
public abstract class AbstractCarClass {

    private final String vin;

    AbstractCarClass(String vin) {
        this.vin = vin;
    }

    public String getVin() {
        return vin;
    }

    public static void doSomething() {
        System.out.println("Do smth");
    }

    // metoda abstracta
    public abstract void runEngine();

}
