package com.sda.fundamentals.oop.abstractClass.exercises;

public class Text {
    /**
     * 1. Creati o clasa abstracta Vehicle care sa aibe field-ul maxSpeed de tipul intreg. Creati un getter pentru acest field si o metoda abstracta move().
     * Creati o alta clasa Car care sa extinda clasa Vehicle si sa ofera o implementare concreta pentru metoda move() - afiseaza ceva in consola.
     * Testati metoda move dintr-o clasa de test care contine metoda main();
     *
     */
}
