package com.sda.fundamentals.oop.recap.ex3;

public interface GeometricObject {

    double getPerimeter();
    double getArea();

}
