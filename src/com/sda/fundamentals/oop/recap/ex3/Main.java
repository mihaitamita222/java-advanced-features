package com.sda.fundamentals.oop.recap.ex3;

/**
 * Implement the classes from the class diagram found in the current package.
 * Write the interface called GeometricObject, which declares two abstract methods: getParameter()
 * and getArea(), as specified in the class diagram.
 * Write the implementation class Circle, with a protected variable radius, which implements the interface
 * GeometricObject.
 * Write a test program called TestCircle to test the methods defined in Circle.
 * The class ResizableCircle is defined as a subclass of the class Circle, which also implements an interface
 * called Resizable, as shown in class diagram. The interface Resizable declares an abstract method
 * resize(), which modifies the dimension ﴾such as radius﴿ by the given percentage. Write the interface
 * Resizable and the class ResizableCircle
 * Write a test program called TestResizableCircle to test the methods defined in ResizableCircle
 */
public class Main {
}
