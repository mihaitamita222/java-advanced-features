package com.sda.fundamentals.oop.recap.ex3;

public class ResizableCircle extends  Circle implements Resizable {

    // Constructor
    public ResizableCircle(double radius) {
        super(radius);
    }

    // Implement methods defined in the interface Resizable
    @Override
    public double resize(int percent) {
        return 0;
    }
}
