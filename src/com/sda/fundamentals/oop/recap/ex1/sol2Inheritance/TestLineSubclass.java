package com.sda.fundamentals.oop.recap.ex1.sol2Inheritance;

import com.sda.fundamentals.oop.recap.ex1.Point;

public class TestLineSubclass {

    public static void main(String[] args) {
        LineSubclass l1 = new LineSubclass(0, 0, 3, 4);
        System.out.println(l1);
        Point p1 = new Point(10, 20);
        Point p2 = new Point(1, 10);
        LineSubclass l2 = new LineSubclass(p1, p2);
        System.out.println(l2);
    }

}
