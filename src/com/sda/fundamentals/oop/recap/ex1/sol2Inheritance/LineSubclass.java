package com.sda.fundamentals.oop.recap.ex1.sol2Inheritance;

import com.sda.fundamentals.oop.recap.ex1.Point;

public class LineSubclass extends Point {

    // A line needs two points: begin and end.
    // The begin point is inherited from its superclass Point

    private Point end;

    // Constructors
    public LineSubclass (int beginX, int beginY, int endX, int endY) {
        super(beginX, beginY); // construct the begin Point
        this.end = new Point(endX, endY); // construct the end Point
    }
    public LineSubclass (Point begin, Point end) { // caller to construct the Points
        super(begin.getX(), begin.getY()); // need to reconstruct the begin Point
        this.end = end;
    }

    public Point getBegin() {
        return new Point(super.getX(), super.getY());
    }

    public Point getEnd() {
        return this.end;
    }

    public void setBegin(Point begin) {
        super.setX(begin.getX());
        super.setY(begin.getY());
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public int getBeginX() {
        return super.getX();
    }
    public int getBeginY() {
        return super.getY();
    }
    public int getEndX() {
        return this.end.getX();
    }
    public int getEndY() {
        return this.end.getY();
    }
    public void setBeginX(int x) {
        super.setX(x);
    }
    public void setBeginY(int y) {
        super.setY(y);
    }
    public void setBeginXY(int x, int y) {
        super.setX(x);
        super.setY(y);
    }
    public void setEndX(int x) {
        this.end.setX(x);
    }
    public void setEndY(int y) {
        this.end.setY(y);
    }
    public void setEndXY(int x, int y) {
        this.end.setX(x);
        this.end.setY(y);
    }

    // Length of the line
    // Math.sqrt((x2-x1)^2 + (y2-y1)^2) <=> Math.sqrt(xDiff*xDiff + yDiff*yDiff)
    public int getLength() {
        int xDiff = end.getX() - super.getX();
        int yDiff = end.getY() - super.getY();
        int sumDiffsSquared = xDiff*xDiff + yDiff*yDiff;
        return (int)Math.sqrt(sumDiffsSquared);
    }

    // Gradient in radians
    // Math.atan2(yDiff, xDiff)
    public double getGradient() {
        int xDiff = end.getX() - super.getX();
        int yDiff = end.getY() - super.getY();
        return Math.atan2(yDiff, xDiff);
    }

    @Override
    public String toString() {
        return "LineSubclass{" +
                "begin=" + super.toString() +
                " end=" + end +
                '}';
    }
}
