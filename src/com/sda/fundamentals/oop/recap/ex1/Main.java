package com.sda.fundamentals.oop.recap.ex1;

/**
 * Ex1. Composition and Inheritance
 * Pornind de la afirmatia: "O linie este compusa din 2 puncte", care poate fi echivalenta cu: "o linie este un punct extins printr-un alt punct"
 * implementati clasele necesare care sa va permita sa trasati o linie din punctul A in punctul B.
 * Avand in vedere conceptele invatate, care sunt modalitatile prin care puteti face o astfel de implementare.
 *Note: un punct are doua coordonate in sistemul 2d (un punct pe axa x, unul pe axa y)
 * O linie este formata din 2 puncte, a si b
 *
 * Implementati getLength si getGradient methods in clasa Line.
 *
 * Summary: Exista doua abordari prin care poti sa creezi o linie, composition sau inheritance.
 * "O linie se compune din doua puncte" sau "O linie este un punct extins cu un alt punct".
 * Compara cele doua clase Line si LineSubclass: Line foloseste composition si LineSubclass inherintance ul
 * Care design este mai bun?
 *
 * Faceti superclasa Point final.
 * Ce se intampla?
 */
public class Main {
    public static void main(String[] args) {

    }
}
