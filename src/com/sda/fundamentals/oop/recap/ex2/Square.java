package com.sda.fundamentals.oop.recap.ex2;

public class Square extends Rectangle {

    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(double side, Color color, boolean filled) {
        super(side, side, color, filled);
    }

    public double getSide() {
//        return super.width;
        return super.length; // the width and the length are equals
    }

    public void setSide(double side) {
        super.width = side;
        super.length = side;
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }

    @Override
    public String toString() {
        return super.toString() +
                ';' +
                "Square{" +
                "side=" + super.width +
                '}' +
                ';' +
                "Area=" +
                super.getArea();
    }
}
