package com.sda.fundamentals.oop.recap.ex2;

/**
 * Implementati ierarhia de clase din diagrama din imaginea atasata
 * # - means protected
 *  + - means public
 *  Definiti un enum de tipul Color
 *
 *  La ce folosesc clasele abstracte si metodele abstracte?
 *
 *  Exercise polymorphism
 *  Exercise circle equality -> compare the results before implementing equals() method and after; how to add also the color to be evaluated when comparing the objects?
 *  Discuss about instance of
 */
public class Main {

    public static void main(String[] args) {
        Shape s1 = new Circle(5.5, Color.RED, false); // Upcast Circle to Shape
        System.out.println(s1); // which version?
        System.out.println(s1.getArea()); // which version?
        System.out.println(s1.getPerimeter()); // which version?
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
//        System.out.println(s1.getRadius());  // de ce nu putem accesa?
        Circle c1 = (Circle)s1; // Downcast back to Circle
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());  // perfect ok, deoarece acum avem un obiect de tipul Circle
//        Shape s2 = new Shape();  // won't compile , why? Shape is abstract and cannot be instantiated
        Shape s3 = new Rectangle(1.0, 2.0, Color.RED, false); // Upcast
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());
//        System.out.println(s3.getLength());  // Shape doesn;t have length, only Rectangle and Square
        Rectangle r1 = (Rectangle)s3; // downcast
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());
        Shape s4 = new Square(6.6); // Upcast
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
//        System.out.println(s4.getSide());  // Shape does not have side
// Take note that we downcast Shape s4 to Rectangle,
// which is a superclass of Square, instead of Square
        Rectangle r2 = (Rectangle)s4;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
//        System.out.println(r2.getSide()); // r2 is of type Rectangle -> does not have side property
        System.out.println(r2.getLength());
// Downcast Rectangle r2 to Square
        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());

        // Using Polymorphism
        Shape shape1 = new Rectangle(10, 20, Color.BLACK, true);
        System.out.println(shape1);
        Shape shape2 = new Circle(10, Color.RED, false);
        System.out.println(shape2);
        Shape shape3 = new Square(3, Color.BLUE, true);
        System.out.println(shape3);

        Shape shape4 = new Circle(10, Color.RED, false);
        System.out.println("Shape 2 and 4 are equals: " + shape2.equals(shape4));

    }

}
