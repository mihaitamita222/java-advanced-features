//jdewndjkwnedjksnwekjsdndjds
//djwendkje
/**
 * dmkjsdnkd
 */
package com.sda.fundamentals.oop.packagesExample;


import com.sda.fundamentals.oop.packagesExample.example.Test;

public class Main {

    public static void main(String[] args) {
        Test test = new Test();
        test.runTest();
//        test.runTestDefaultAccess();  // invalid because we are not in the same package
//        test.runTestProtectedAccess();  // invalid because we are not in the same package and Main class is not a subclass of test package
        com.sda.fundamentals.oop.packagesExample.example2.Test tes1 = new com.sda.fundamentals.oop.packagesExample.example2.Test();
        tes1.runTest();
    }

    public static void print() {
        System.out.println();
    }

}
