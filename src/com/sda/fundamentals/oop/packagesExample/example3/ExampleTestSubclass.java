package com.sda.fundamentals.oop.packagesExample.example3;

import com.sda.fundamentals.oop.packagesExample.example.Test;

// subclass of Test class
public class ExampleTestSubclass extends Test {

    void test() {
        super.runTestProtectedAccess();
    }

}
