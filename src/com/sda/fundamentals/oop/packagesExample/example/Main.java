package com.sda.fundamentals.oop.packagesExample.example;

public class Main {
    public static void main(String[] args) {
        Test test = new Test();
        test.runTestDefaultAccess();
        test.runTestProtectedAccess();
    }
}
