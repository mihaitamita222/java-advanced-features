package com.sda.fundamentals.oop.interfaces;

public class InterfaceImplementation implements InterfaceExample {

    @Override
    public String doSomething() {
        return "do something method";
    }

    @Override
    public void executeSomeCode() {
        System.out.println("Execute Some Code");
    }

}
