package com.sda.fundamentals.oop.interfaces;

public class Test {
    public static void main(String[] args) {
//        InterfaceExample interfaceExampleObj = new InterfaceExample(); // invalid code - interface cannot be instantiated
        InterfaceImplementation impl = new InterfaceImplementation();
        impl.doSomething();
        impl.defaultMethodWithBody();
        InterfaceExample interfaceExampleObj = new InterfaceImplementation();
        interfaceExampleObj.doSomething(); // calling concrete implementation
        interfaceExampleObj.defaultMethodWithBody();
    }
}
