package com.sda.fundamentals.oop.interfaces;

public abstract class Animal extends GenericAnimal {

    public abstract void legs();

    @Override
    public void food() {

    }
}
