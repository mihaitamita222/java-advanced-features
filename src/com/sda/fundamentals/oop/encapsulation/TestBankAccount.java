package com.sda.fundamentals.oop.encapsulation;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount("Ro06jnsjdnsajd");
        bankAccount.setAccountPassword("thisismysecretpassword");
        bankAccount.setSum(100.0);

//        String pass = bankAccount.accountPassword;

        bankAccount.getIban();
        bankAccount.getSum();
    }
}
