package com.sda.fundamentals.oop.encapsulation;

public class TestDog {
    public static void main(String[] args) {
        Dog dog = new Dog("female", "race1");
        System.out.println(dog.getAge());
        System.out.println(dog.getWeight());
    }
}
