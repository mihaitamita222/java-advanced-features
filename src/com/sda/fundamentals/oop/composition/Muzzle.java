package com.sda.fundamentals.oop.composition;

public class Muzzle {
    private String type;

    public Muzzle(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
