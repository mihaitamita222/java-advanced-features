package com.sda.fundamentals.oop.composition;

public class Test {
    public static void main(String[] args) {
        Muzzle muzzle = new Muzzle("steel");
        Dog dog = new Dog("name", "red", muzzle);

        System.out.println(dog.getMuzzle().getType());
    }
}
