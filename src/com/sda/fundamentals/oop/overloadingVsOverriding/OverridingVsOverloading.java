package com.sda.fundamentals.oop.overloadingVsOverriding;

import java.util.Objects;

public class OverridingVsOverloading {

    private String name;
    private String a;
    private String b;
    int no;


    // constructor overloading

    OverridingVsOverloading() {
    }

    OverridingVsOverloading(String a) {
        this.a = a;
    }

    OverridingVsOverloading(String a, int no) {
        this.a = a;
        this.no = no;
    }

    OverridingVsOverloading(int no, String a) {
        this.a = a;
        this.no = no;
    }

    OverridingVsOverloading(String a, String b) {
        this.a = a;
        this.b = b;
    }

    OverridingVsOverloading(String a, String b, int no) {
        this.a = a;
        this.b = b;
        this.no = no;
    }

    // method overloading -> metoda are acelasi nume si acelasi tip returnat, difera lista de parametrii

    void overloadedMethod() {
    }

    void overloadedMethod(int a) {
    }

    void overloadedMethod(int a, int b) {
    }

//    void overloadedMethod(int c, int d) {
//    }

    void overloadedMethod(int a, String b) {
    }

    void overloadedMethod(String a, int b) {
    }

//    void overloadedMethod(String c, int d) {
//    }

    int add(int a, int b) {
        return a + b;
    }

    // not a valid case of overloading
//    double add(int a, int b) {
//        return a + b;
//    }

    // not a valid case of overloading
    double add(int a, int b, int c) {
        return a +b +c;
    }

    int add(int a, int b, int c, int d) {
        return a + b + c + d;
    }

    // method overridden -> from the parent class
    // suprascriem metode din clasa parinte
    @Override
    public String toString() {
        return super.toString();
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof OverridingVsOverloading)) return false;
//        OverridingVsOverloading that = (OverridingVsOverloading) o;
//        return name.equals(that.name);
//    }

    //    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof OverridingVsOverloading)) return false;
//        OverridingVsOverloading that = (OverridingVsOverloading) o;
//        return no == that.no && Objects.equals(a, that.a) && Objects.equals(b, that.b);
//    }



    @Override
    public int hashCode() {
        return Objects.hash(a, b, no);
    }
}
