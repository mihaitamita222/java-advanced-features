package com.sda.fundamentals.oop.homework.ex3;

/**
 * Ex 3
 * Create a super class called Car. The Car class has the following fields and methods.
 * int speed;
 * double regularPrice;
 * String color;
 * double getSalePrice();
 * Create a subclass of Car class and name it as Truck. The Truck class has the following fields and methods.
 * int weight;
 * double getSalePrice();   //Ifweight>2000,10%discount.Otherwise,20%discount.
 *
 * Create a subclass of Car class and name it as Ford. The Ford class has the following fields and methods
 * int year;
 * int manufacturerDiscount;
 * double getSalePrice(); // From the sale price computed from Car class, subtract the manufacturer Discount.
 * Create a subclass of Car class and name it as Sedan. The Sedan class has the following fields and methods.
 * int length;
 * double getSalePrice(); // Ilength>20feet, 5%discount, otherwise, 10%discount
 *
 * Create MyOwnAutoShop class which contains the main() method. Perform the following within the main() method.
 * Create an instance of Sedan class and initialize all the fields with appropriate values. Use super(...) method in the constructor for initializing the fields of the superclass.
 * Create two instances of the Ford class and initialize all the fields with appropriate values. Use super(...) method in the constructor for initializing the fields of the super class
 * Create an instance of Car class and initialize all the fields with appropriate values. Display the sale prices of all instance.
 */
public class MyOwnAutoShop {

    public static void main(String[] args) {
        Sedan sedan = new Sedan(180, 18_000, "RED", 2);
        System.out.println(sedan.getSalePrice());
        Ford ford1 = new Ford(145, 15_000, "WHITE", 2015, 2000);
        System.out.println(ford1.getSalePrice());
        Ford ford2 = new Ford(210, 17_000, "BLUE", 2019, 2000);
        System.out.println(ford2.getSalePrice());
        Car car = new Car(260, 20_000, "BLACK");
        System.out.println(car.getSalePrice());
    }

}
