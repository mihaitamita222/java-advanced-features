package com.sda.fundamentals.oop.inheritance;

public class Test {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 20);
        rectangle.getX();
        rectangle.getHeight();
        Circle circle = new Circle(10);
        circle.getX();
        circle.getRadius();

        // polymorfism
        Shape[] shapes = new Shape[2];
        shapes[0] = rectangle;
        shapes[1] = circle;

        for (int i = 0; i < 2; i++) {
            shapes[i].setColor("red");
        }

        doSomething(rectangle);
        doSomething(circle);

        System.out.println(circle);
        System.out.println(rectangle);

        Shape shape = new Shape();
//        shape.getRadius();  // invalid

    }

    // method that accepts an argument of type Shape
    private static void doSomething(Shape shape) {
        shape.move(10, 20);
        System.out.println("After moving to (x,y) position; new points are: " + shape.getX() + "and " + shape.getY());
    }

}
