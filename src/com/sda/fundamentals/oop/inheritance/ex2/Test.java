package com.sda.fundamentals.oop.inheritance.ex2;

public class Test {
    public static void main(String[] args) {
        Cat cat = new Cat(false, "milk", 4, "black", "Miky");
        Dog dog = new Dog(true, "food", 4, "white", "Arthur");

        System.out.println("Cat is Vegetarian?" + cat.isVegetarian());
        System.out.println("Cat eats " + cat.getEats());
        System.out.println("Cat has " + cat.getNoOfLegs() + " legs.");
        System.out.println("Cat color is " + cat.getColor());

        Animal[] animals = new Animal[2];
        animals[0] = cat;
        animals[1] = dog;

        for(Animal animal: animals) {
            animal.yieldVoice();
        }

    }
}
