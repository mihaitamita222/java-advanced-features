package com.sda.fundamentals.overall_exercise;

import java.io.Serializable;

public abstract class Employee implements Serializable {

    private String name;
    private String telephoneNumber;
    private String tool;

    private volatile double earnings;
    private volatile double earningPerHour;

    public Employee() {
        System.out.println("Constructor clasa Employee");
    }

    public Employee(String name, String telephoneNumber, String tool, Double earningsPerHour) {
        this.name = name;
        this.telephoneNumber = telephoneNumber;
        this.tool = tool;
        this.earningPerHour = earningsPerHour;
        System.out.println("Constructor clasa Employee");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getTool() {
        return tool;
    }

    public double getEarnings() {
        return this.earnings;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", tool='" + tool + '\'' +
                ", earnings=" + earnings +
                '}';
    }

    public void increaseEarnings() {
        this.earnings += this.earningPerHour;
    }
}
