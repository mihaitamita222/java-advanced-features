package com.sda.fundamentals.overall_exercise;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Folositi principiile OOP cunoscute, creati o fabrica de angajati care sa includa muncitori de diferite tipuri, si anume:
 *  manageri (Manager), muncitori(Worker) si directori(Director).
 *  Hint: Toti angajatii, indiferent de natura lor au urmatoarele caracteristici:
 *  name, telephoneNumber, tool, earningPerHour
 *  Clasa comuna ar trebui sa aibe o metoda nu numele: increaseEarnings() - care sa permita incrementarea castigurilor per ora ale angajatilor
 *  Metoda increaseEarnings trebuie apelata o data la fiecare ora, pentru a obtine castigurile totale ale angajatilor de la inceputul unei zile pana la final de zi.
 *  Folosind o clasa de test, implementati urmatoarele functionalitati - createDirector, createManager, createWorker, showEmployees; importEmployees, exportEmployees.
 *  Metoda de export trebuie sa ruleze in paralel cu firul principal de executie al programului.
 *  Apelati metodele care va permit sa creeati angajati din metoda main si initializati fabrica de angajati cu rezultatul lor.
 *  Creati o clasa Budget in care sa putem stoca diferite valute (RON, EUR, USD).
 *  Aceasta clasa o sa ne ajute sa determinam bugetul total an fabricii si sa afisam valuarea cu valuta corespunzatoare.
 *  Calculul bugetului sa se face pe un thread separat.
 */
public class Main {

    private static final List<Employee> employees = new ArrayList<>();
    private static final Random random = new Random();
    private static final Budget<Currency> factoryBudget = new Budget<>(Currency.EUR);

    public static void main(String[] args) throws InterruptedException {
        createDirector();
        createManager();
        createWorker();

        startWorking();

        showEmployees();

        factoryBudget.displayTotalBudget(employees);

        exportEmployees();
        importEmployees();
        showEmployees();


    }

    private static void startWorking() {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    employees.forEach(Employee::increaseEarnings);
                    Thread.sleep(10000 * 3); // o data pe ora, marim castigul (deoarece preturile sunt pe ora)
                } catch (InterruptedException e) {
                    System.err.println(e.getMessage());
                }
            }
        });

        thread.start();
    }

    private static void createWorker() {
        System.out.println("Worker");
        int workerNo = random.nextInt(1, 500);
        employees.add(new Worker("Worker" + workerNo, "+4" + workerNo, "Worker tool " + workerNo, random.nextDouble(10.0, 50.0)));
    }

    private static void createManager() {
        System.out.println("Manager");
        int managerNo = random.nextInt(500, 1000);
        employees.add(new Manager("Manager" + managerNo, "+4" + managerNo, "Manager tool " + managerNo, random.nextDouble(50.0, 100.0)));
    }

    private static void createDirector() {
        System.out.println("Director");
        int directorNo = random.nextInt(1000, 5000);
        employees.add(new Director("Director" + directorNo, "+4" + directorNo, "Director tool " + directorNo, random.nextDouble(100.0, 500.0)));
    }

    private static void showEmployees() {
        for (Employee employee : employees) {
            //instanceof =>ne ajuta sa identificam clasa cu care a fost creat obiectul
            if (employee instanceof Director) {
                System.out.println("Director: " + employee.getName() + " - " + employee.getTelephoneNumber() + " - " + employee.getTool() + " - " + employee.getEarnings());
            } else if (employee instanceof Manager) {
                System.out.println("Manager: " + employee.getName() + " - " + employee.getTelephoneNumber() + " - " + employee.getTool() + " - " + employee.getEarnings());
            } else if (employee instanceof Worker) {
                System.out.println("Worker: " + employee.getName() + " - " + employee.getTelephoneNumber() + " - " + employee.getTool() + " - " + employee.getEarnings());
            }

        }
    }

    private static void exportEmployees() {
        Runnable functionThread = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000 * 3);
                    exportData(employees);

                } catch (Exception e) {
                    System.err.println(e.getMessage());

                }
            }
        };
        Thread exportThread = new Thread(functionThread);
        exportThread.start();
    }

    private static void importEmployees() {
        List<Employee> temp = importData("employees.txt");
        for (Employee emp : temp) {
            employees.add(emp);
        }
    }

    private static void exportData(List<Employee> employesList) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("employees.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(employesList);

            objectOutputStream.close();
            System.out.println("Export with success!");
        } catch (Exception e) {
            //err =>afiseaza textul cu rosu in consola
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private static List<Employee> importData(String fileName) {
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            List<Employee> employees = (List<Employee>) objectInputStream.readObject();
            System.out.println("Import with success!");
            return employees;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Collections.EMPTY_LIST;
        }
    }

}